<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'ralecatrade' );

/** MySQL database username */
define( 'DB_USER', 'ralecatrade' );

/** MySQL database password */
define( 'DB_PASSWORD', 'p.x047S6@k' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '0aiqrwlefogypybppkr1hsidsc7rafk7clyvub5qxsthkelxi3857lgbbanned83' );
define( 'SECURE_AUTH_KEY',  'rl7rrjakju32esweubdv65jbbgmzx9tzj00fauy2oymh2ncfx80clw1cpqgypsg7' );
define( 'LOGGED_IN_KEY',    'lak8cy6euaq8bghy1rhgtnfaciu709acfy2h1e4waxjgyawrycbvt44gplecnppy' );
define( 'NONCE_KEY',        'lwvh8zuy7bw7v5bzvtgvjj4rmfdlk8b7ndyprxdxz3mqp8ewd8vstd4ekxzefpzj' );
define( 'AUTH_SALT',        'wqwmlgysrn6jino9xrirqwch9dhiwbknnekgizjll9r0aenq4kozc4wtauqbrf8y' );
define( 'SECURE_AUTH_SALT', 'wp2z6hw4eugxou5ozifwquybr0ziku5tbur5ybbs8h788hudlqn4sg0vxaxmeawx' );
define( 'LOGGED_IN_SALT',   'qmb7mryluy897xn7lz1tacuawgfgvadertw0na5m0eouatrvf6jh1cztkflk3tde' );
define( 'NONCE_SALT',       'upfwxyqi7wnba5f5vdowxq8rj5w7xr0cr1upawzfbvzptee4buhaqveatgbimbap' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wphw_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
